<?php

/**
 * @file
 * Contains hug_annuaire.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\group_permissions\Entity\GroupPermission;

/**
 * Implements hook_help().
 */
function group_permissions_template_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the hug_annuaire module.
    case 'help.page.group_permissions_template':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Create templates for group permissions and apply to a specific group') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_entity_bundle_create().
 *
 * Add the field permission_templates when creating a new Group type.
 */
function group_permissions_template_entity_bundle_create($entity_type_id, $bundle) {
  if ($entity_type_id === 'group') {
    /** @var Drupal\group_permissions_template\Service\PermissionTemplatesService $permission_templates_service */
    $permission_templates_service = \Drupal::service('group_permissions_template.permission_templates');
    $permission_templates_service->fieldInstall($bundle);
  }
}

/**
 * Implements hook_form_group_form_alter().
 *
 * Remove bad choices for field permission_templates in widgets.
 */
function group_permissions_template_form_group_form_alter(&$form, &$form_state, $form_id) {
  $group_bundle = '';

  // Get group bundle at Group edition.
  $current_group = \Drupal::request()->get('group');
  if (is_object($current_group)) {
    $group_bundle = $current_group->bundle();
  }

  // Get group bundle at Group creation.
  $group_type = \Drupal::request()->get('group_type');
  if (is_object($group_type)) {
    $group_bundle = $group_type->id();
  }

  // Get group bundle at Subgroup creation.
  /** @var \Drupal\Core\Routing\CurrentRouteMatch $route_match */
  $route_match = \Drupal::routeMatch();
  if ($route_match->getRouteName() === 'entity.group_content.create_form') {
    if ($plugin_id = $route_match->getParameter('plugin_id')) {
      /** @var \Drupal\group\Plugin\GroupContentEnablerManager $group_content_enabler */
      $plugin_definition = \Drupal::service('plugin.manager.group_content_enabler')
        ->getDefinition($plugin_id, FALSE);
      $plugin_definition_id = $plugin_definition['id'] ?? NULL;
      if ($plugin_definition_id === 'subgroup') {
        $group_bundle = $plugin_definition['entity_bundle'];
      }
    }
  }
  if (isset($form['group_permission_template']['widget']) && !empty($group_bundle)) {
    // Case of widget is options_select or options_buttons.
    if (isset($form['group_permission_template']['widget']['#options'])) {
      $template_ids = array_keys($form['group_permission_template']['widget']['#options']);
      $templates = \Drupal\group_permissions_template\Entity\GroupPermissionTemplate::loadMultiple($template_ids);
      foreach($templates as $template_id => $template) {
        if (is_object($template) && $group_bundle !== $template->getType()) {
          unset($form['group_permission_template']['widget']['#options'][$template_id]);
        }
      }
    }
    // Case of widget is entity_reference_autocomplete.
    elseif(isset($form['group_permission_template']['widget'][0]) && isset($form['group_permission_template']['widget'][0]['target_id'])) {
      $form['group_permission_template']['widget'][0]['target_id']['#selection_settings']['group_bundle'] = $group_bundle;
    }
    // Case of widget is entity_reference_autocomplete_tags.
    elseif (isset($form['group_permission_template']['widget']['target_id'])) {
      $form['group_permission_template']['widget']['target_id']['#selection_settings']['group_bundle'] = $group_bundle;
    }
  }
}

/**
 * Implements hook_entity_presave().
 *
 * Override rights of a specific group in regards of selected template in field group_permission_template.
 */
function group_permissions_template_entity_presave(Drupal\Core\Entity\EntityInterface $group) {
  // check entity is a group.
  if (!empty($group) && $group->getEntityTypeId() === 'group') {
    // check if field group_permission_template exists in group.
    if (!empty($group->group_permission_template) && !empty($group->original)) {
      if ($group->group_permission_template->target_id !== $group->original->group_permission_template->target_id) {
        if (empty($group->group_permission_template->target_id)) {
          // Remove group permissions if exists.
          /** @var Drupal\group_permissions_template\Service\PermissionTemplatesService $permission_templates_service */
          $permission_templates_service = \Drupal::service('group_permissions_template.permission_templates');
          $permission_templates_service->unsetGroupPermissionsToGroup($group);
        }
        else {
          // Set group permissions from a template
          /** @var Drupal\group_permissions_template\Service\PermissionTemplatesService $permission_templates_service */
          $permission_templates_service = \Drupal::service('group_permissions_template.permission_templates');
          $permission_templates_service->setTemplateAsGroupPermissions($group);
        }
      }
    }
  }
}

/**
 * Implements hook_entity_insert().
 *
 * Override rights of a created group in regards of selected template in field group_permission_template.
 */
function group_permissions_template_group_insert(Drupal\Core\Entity\EntityInterface $group) {
  if (!empty($group->group_permission_template) && !empty($group->group_permission_template->target_id)) {
    // Set group permissions from a template
    /** @var Drupal\group_permissions_template\Service\PermissionTemplatesService $permission_templates_service */
    $permission_templates_service = \Drupal::service('group_permissions_template.permission_templates');
    $permission_templates_service->setTemplateAsGroupPermissions($group);
  }
}
