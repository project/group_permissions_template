<?php

namespace Drupal\group_permissions_template\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines a Group permission template entity.
 *
 * @ConfigEntityType(
 *   id = "group_permission_template",
 *   label = @Translation("Group permission template"),
 *   handlers = {
 *     "list_builder" = "Drupal\group_permissions_template\Controller\GroupPermissionTemplateListBuilder",
 *     "form" = {
 *       "add" = "Drupal\group_permissions_template\Form\GroupPermissionsTemplateForm",
 *       "delete" = "Drupal\group_permissions_template\Form\GroupPermissionsTemplateDeleteForm",
 *     }
 *   },
 *   config_prefix = "group_permission_template",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "type" = "Group type",
 *     "permissions" = "permissions",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "type",
 *     "permissions",
 *   },
 *   links = {
 *     "edit-form" = "/admin/group/group_permissions_template/{group_permission_template}",
 *     "delete-form" = "/admin/group/group_permissions_template/{group_permission_template}/delete",
 *   }
 * )
 */

class GroupPermissionTemplate extends ConfigEntityBase implements GroupPermissionTemplateInterface {

  /**
   * The template ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The template title.
   *
   * @var string
   */
  protected $label;

  /**
   * The group type.
   *
   * @var string
   */
  protected $type;

  /**
   * The template permissions.
   *
   * @var array
   */
  protected $permissions;

  /**
   * {@inheritdoc}
   */
  public function setLabel(string $label) {
    $this->label = $label;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->type ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setType(string $type) {
    $this->type = $type;
  }

    /**
   * {@inheritdoc}
   */
  public function getPermissions($format = self::GROUP_FORMAT) {
    $permissions = $this->permissions;
    if ($format === self::GROUP_PERMISSIONS_FORMAT) {
      foreach($permissions as $role_name => $permission) {
        $permissions[$role_name] = array_keys(array_filter($permission));
      }
    }
    return $permissions;
  }

  /**
   * {@inheritdoc}
   */
  public function setPermissions(array $permissions) {
    $this->permissions = $permissions;
  }

}
