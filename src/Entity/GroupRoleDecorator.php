<?php

namespace Drupal\group_permissions_template\Entity;

use Drupal\group\Entity\GroupRole;

class GroupRoleDecorator extends GroupRole implements GroupRoleDecoratorInterface {

  /**
   * The decorated GroupRole entity.
   *
   * @var \Drupal\group\Entity\GroupRole $group_role
   */
  protected $groupRole;

  /**
   * {@inheritdoc}
   */
  public function setGroupRole(GroupRole $group_role) {
    if ($group_role->isOutsider() && !$group_role->inPermissionsUI()) {
      $this->setWeight(200);
      $group_role->setWeight(200);
    }
    else {
      $this->setWeight($group_role->getWeight());
    }
    $this->permissions = $group_role->getPermissions();
    $this->groupRole = $group_role;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupRole() {
    return $this->groupRole;
  }

  /**
   * {@inheritdoc}
   */
  public function __call($method, $args) {
    if (is_callable(GroupRole::class, $method)) {
      call_user_func_array(array($this->groupRole, $method), $args);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    $label = $this->groupRole->label();
    if ($this->groupRole->isOutsider() && !$this->groupRole->inPermissionsUI()) {
      $label .= ' (Outsider)';
    }
    return $label;
  }
}
