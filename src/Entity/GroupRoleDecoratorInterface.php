<?php

namespace Drupal\group_permissions_template\Entity;

use Drupal\group\Entity\GroupRole;

interface GroupRoleDecoratorInterface {

  /**
   * Set the GroupRole entity.
   *
   * @param \Drupal\group\Entity\GroupRole $group_role
   *    The decorated GroupRole entity.
   */
  public function setGroupRole(GroupRole $group_role);

  /**
   * Get the GroupRole entity.
   *
   */
  public function getGroupRole();
}
