<?php

namespace Drupal\group_permissions_template\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Group permission template entities.
 *
 */
interface GroupPermissionTemplateInterface extends ConfigEntityInterface {

  /**
   * Format key to store permissions
   */
  const GROUP_FORMAT = 'group';
  const GROUP_PERMISSIONS_FORMAT = 'group_permissions';

  /**
   * Sets the label of template.
   *
   * @param string $label
   *   The string label.
   */
  public function setLabel(string $label);

  /**
   * Gets template permissions.
   *
   * @return array
   *   Permissions.
   */
  public function getPermissions();

  /**
   * Sets the template permissions.
   *
   * @param array $permissions
   *   Group permissions.
   */
  public function setPermissions(array $permissions);

  /**
   * Get the group type (used to apply the template).
   *
   * @return string
   *    Group type.
   */
  public function getType();

  /**
   * Set the group type (used to apply the template).
   *
   * @param string $type
   *    Group type.
   */
  public function setType(string $type);

}
