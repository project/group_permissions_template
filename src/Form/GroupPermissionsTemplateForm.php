<?php

namespace Drupal\group_permissions_template\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityForm;

/**
 * Provides the group permissions administration form.
 */
class GroupPermissionsTemplateForm extends EntityForm {

  /**
   * The Drupal entityTypeManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Drupal FormBuilder service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * GroupPermissionsTemplateForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *    The Drupal entityTypeManager service.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *    The Drupal FormBuilder service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, FormBuilderInterface $form_builder) {
    $this->entityTypeManager = $entityTypeManager;
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(\Symfony\Component\DependencyInjection\ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('form_builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getGroupRoles($group_type_id) {
    $properties = [
      'group_type' => $group_type_id,
      'permissions_ui' => TRUE,
    ];

    return $this->entityTypeManager
      ->getStorage('group_role')
      ->loadByProperties($properties);
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (!$this->entity->isNew()) {
      $form_state->setRedirect('entity.group_permission_template.add_form_permissions', ['group_type' => 'service']);
    }
    else {
      $group_types = $this->getGroupTypes();
      $form['group_type'] = [
        '#title' => $this->t('Group types'),
        '#type' => 'select',
        '#options' => $group_types,
        '#required' => TRUE
      ];
      $form['action'] = [
        '#value' => $this->t('Go to permissions'),
        '#type' => 'submit'
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $group_type = $form_state->getValue('group_type');
    $form_state->setRedirect('entity.group_permission_template.add_form_permissions', ['group_type' => $group_type]);
  }

  /**
   * Get all group types.
   *
   * @return array
   *    List of group types.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getGroupTypes() {
    $types = [];
    $group_types = $this->entityTypeManager
      ->getStorage('group_type')->loadMultiple();
    foreach($group_types as $group) {
      $types[$group->id()] = $group->label();
    }
    return $types;
  }
}
