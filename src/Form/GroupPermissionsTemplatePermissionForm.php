<?php

namespace Drupal\group_permissions_template\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Access\GroupPermissionHandlerInterface;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group\Form\GroupPermissionsTypeSpecificForm;
use Drupal\group_permissions_template\Entity\GroupPermissionTemplateInterface;
use Drupal\group_permissions_template\Entity\GroupRoleDecorator;
use Drupal\group_permissions_template\Service\PermissionTemplatesInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the group permissions template administration form.
 */
class GroupPermissionsTemplatePermissionForm extends GroupPermissionsTypeSpecificForm {

  /**
   * GroupPermissionTemplate entity.
   *
   * @var GroupPermissionTemplateInterface
   */
  protected $groupPermissionTemplate;

  /**
   * Permission template service.
   *
   * @var \Drupal\group_permissions_template\Service\PermissionTemplatesInterface
   */
  protected $permissionTemplateService;

  /**
   * Constructs a new GroupPermissionsTemplatePermissionForm.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\group\Access\GroupPermissionHandlerInterface $permission_handler
   *   The group permission handler.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\group_permissions_template\Service\PermissionTemplatesInterface $permission_template_service
   *   The module handler.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    GroupPermissionHandlerInterface $permission_handler,
    ModuleHandlerInterface $module_handler,
    PermissionTemplatesInterface $permission_template_service
    ) {
    parent::__construct($entity_type_manager, $permission_handler, $module_handler);
    $this->permissionTemplateService = $permission_template_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('group.permissions'),
      $container->get('module_handler'),
      $container->get('group_permissions_template.permission_templates')
    );
  }

  public function getFormId() {
    return 'group_permissions_template.permissions';
  }

  /**
   * Get permissions by roles from entity GroupPermissionTemplate.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]|\Drupal\group\Entity\GroupRoleInterface[]
   *    Array of permissions by role name.
   */
  protected function getGroupRoles() {
    $group = $this->entityTypeManager->getStorage('group')->create([
      'title' => 'dummy',
      'type' => $this->groupType->id()
    ]);
    $group_roles = \Drupal::service('group_permission.group_permissions_manager')->getGroupRoles($group);
    $template_group_roles = [];
    if (!empty($this->groupPermissionTemplate)) {
      foreach($this->groupPermissionTemplate->getPermissions() as $role_name => $permissions) {
        if (isset($group_roles[$role_name])) {
          $group_roles[$role_name]->changePermissions($permissions);
          /** @var \Drupal\group\Entity\GroupRole $group_role */
          $group_role = $group_roles[$role_name];

          // Using a decorator to change labels and sort roles.
          $group_role_decorator = new GroupRoleDecorator([], $group_role->getEntityTypeId());
          $group_role_decorator->setGroupRole($group_role);
          $group_roles[$role_name] = $group_role_decorator;
        }
      }
    }
    return $group_roles;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, GroupTypeInterface $group_type = NULL, $group_permission_template = NULL) {
    if (empty($group_permission_template)) {
      $group_permission_template =  $this->entityTypeManager->getStorage('group_permission_template')->create();
    }
    else {
      $this->groupPermissionTemplate = $this->entityTypeManager->getStorage('group_permission_template')->load($group_permission_template);
      $group_permission_template = $this->groupPermissionTemplate;
    }
    $form['template_label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#default_value' => $group_permission_template->label(),
      '#required' => TRUE
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $group_permission_template->id(),
      '#machine_name' => array(
        'exists' => '\Drupal\group_permissions_template\Entity\GroupPermissionTemplate::load',
      ),
      '#disabled' => !$group_permission_template->isNew(),
    ];
    return parent::buildForm($form, $form_state, $group_type);
  }

  /**
   * {@inheritdoc}
   */
  function submitForm(array &$form, FormStateInterface $form_state) {
    $permissions = [];
    foreach ($this->getGroupRoles() as $role_name => $group_role) {
      $permissions[$role_name] = $form_state->getValue($role_name);
    }
    $group_permission_template = $this->getRequest()->get('group_permission_template');
    if (!empty($group_permission_template)) {
      // Update a GroupPermissionTemplate.
      /** @var GroupPermissionTemplateInterface $group_permission_template */
      $group_permission_template = $this->entityTypeManager->getStorage('group_permission_template')->load($group_permission_template);
      $group_permission_template->setLabel($form_state->getValue('template_label'));
      $group_permission_template->setPermissions($permissions);
      $group_permission_template->save();

      // Update permissions on groups
      $this->permissionTemplateService->updateGroupsPermissions($group_permission_template->id());
    }
    else {
      // Create a GroupPermissionTemplate.
      $group_permission_template = $this->entityTypeManager->getStorage('group_permission_template')->create([
        'id' => $form_state->getValue('id'),
        'label' => $form_state->getValue('template_label'),
        'type' => $this->groupType->id(),
        'permissions' => $permissions
      ]);
      $group_permission_template->save();
    }

    $this->messenger()->addStatus($this->t('The template @template has been saved.', [
      '@template' => $group_permission_template->label()
    ]));
    $form_state->setRedirect('entity.group_permission_template.collection');
  }

}
