<?php

namespace Drupal\group_permissions_template\Service;

use Drupal\group\Entity\Group;

interface PermissionTemplatesInterface  {

  /**
   * Field name and label.
   */
  const TEMPLATE_FIELD_NAME = 'permission_templates';
  const TEMPLATE_FIELD_LABEL = 'Permission Templates';

  /**
   * Install permission_templates field to group_types
   *
   * @param string|null $group_type
   *    The group_type to install the field.
   *    If no group_type is passed, the field is set to all group_types.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function fieldInstall(string $group_type = NULL);

  /**
   * Uninstall permission_templates field.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function fieldUninstall();

  /**
   * Set permissions from a template to a group.
   *
   * @param \Drupal\group\Entity\Group $group
   *    Group entity.
   *
   * @return void
   */
  public function setTemplateAsGroupPermissions(Group $group);

  /**
   * Remote permissions from a group.
   *
   * @param \Drupal\group\Entity\Group $group
   *    Group entity.
   *
   * @return void
   */
  public function unsetGroupPermissionsToGroup(Group $group);

  /**
   * Update all Group permissions linked to the updated template.
   *
   * @param string $group_permission_template_id
   */
  public function updateGroupsPermissions(string $group_permission_template_id);
}
