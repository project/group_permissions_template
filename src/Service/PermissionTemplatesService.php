<?php

namespace Drupal\group_permissions_template\Service;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\group\Entity\Group;
use Drupal\group_permissions\GroupPermissionsManagerInterface;
use Drupal\group_permissions_template\Entity\GroupPermissionTemplateInterface;


class PermissionTemplatesService implements PermissionTemplatesInterface {

  use StringTranslationTrait;

  /**
   * The Drupal entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Group permissions manager from group_permissions module.
   *
   * @var \Drupal\group_permissions\GroupPermissionsManagerInterface
   */
  protected $groupPermissionsManager;

  /**
   * Current active user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $dateTime;

  /**
   * Drupal messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $stringTranslation;

  /**
   * PermissionTemplatesService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *    The Drupal entity type manager service.
   * @param \Drupal\group_permissions\GroupPermissionsManagerInterface $group_permissions_manager
   *    Group permissions manager from group_permissions module.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *    Current active user.
   * @param \Drupal\Component\Datetime\TimeInterface $date_time
   *    Drupal time service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *    Drupal messenger service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    GroupPermissionsManagerInterface $group_permissions_manager,
    AccountProxyInterface $current_user,
    TimeInterface $date_time,
    MessengerInterface $messenger,
    TranslationInterface $string_translation
    ) {

    $this->entityTypeManager = $entity_type_manager;
    $this->groupPermissionsManager = $group_permissions_manager;
    $this->currentUser = $current_user;
    $this->dateTime = $date_time;
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldInstall(string $group_type = NULL) {
    $field_label = self::TEMPLATE_FIELD_LABEL;

    $fsc_storage = $this->entityTypeManager->getStorage('field_storage_config');
    $fc_storage = $this->entityTypeManager->getStorage('field_config');
    $group_type_storage = $this->entityTypeManager->getStorage('group_type');
    $group_types = $group_type_storage->loadMultiple();
    if (!empty($group_type)) {
      $group_types = [$group_type_storage->load($group_type)];
    }
    foreach($group_types as $group_type) {
      $field_storage = $fsc_storage->load('group.group_permission_template');
      $field = $fc_storage->load('group.'.$group_type->id().'.'.self::TEMPLATE_FIELD_NAME);
      if (empty($field)) {
        $fc_storage->create([
          'field_storage' => $field_storage,
          'bundle' => $group_type->id(),
          'label' => $this->t($field_label),
        ])->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fieldUninstall() {
    $fc_storage = $this->entityTypeManager->getStorage('field_config');
    $group_types = $this->entityTypeManager->getStorage('group_type')->loadMultiple();
    foreach($group_types as $group_type) {
      $field = $fc_storage->load('group.'.$group_type->id().'.'.self::TEMPLATE_FIELD_NAME);
      if (!empty($field)) {
        $field->delete();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function unsetGroupPermissionsToGroup(Group $group) {
    /** @var \Drupal\group_permissions\Entity\GroupPermission $group_permission */
    $group_permission = $this->groupPermissionsManager->loadByGroup($group);

    // Unset permissions for the group if exists.
    if (!empty($group_permission)) {
      $group_permission->delete();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setTemplateAsGroupPermissions(Group $group) {
    /** @var \Drupal\group_permissions\Entity\GroupPermission $group_permission */
    $group_permission = $this->groupPermissionsManager->loadByGroup($group);

    // Save a GroupPermission object.
    /** @var \Drupal\Core\Entity\EntityStorageInterface $permission_template_storage */
    $permission_template_storage = $this->entityTypeManager->getStorage('group_permission_template');
    $group_permission_template = $permission_template_storage->load($group->group_permission_template->target_id);
    $permissions = $group_permission_template->getPermissions(GroupPermissionTemplateInterface::GROUP_PERMISSIONS_FORMAT);

    if (empty($group_permission)) {
      $group_permission = $this->entityTypeManager->getStorage('group_permission')->create([
        'gid' => $group->id(),
      ]);
    }
    $group_permission->setPermissions($permissions);
    $violations = $group_permission->validate();
    if (count($violations) == 0) {
      // Set new Revision.
      $group_permission->setNewRevision();
      $group_permission->setRevisionUserId($this->currentUser->id());
      $group_permission->setRevisionCreationTime($this->dateTime->getRequestTime());
      $group_permission->setRevisionLogMessage($this->t('Set Group Permissions Template @template_id', [
        '@template_id' => $group_permission_template->id()
      ]));
      $group_permission->save();
      $this->messenger->addMessage($this->t('The changes have been saved.'));
    }
    else {
      foreach ($violations as $violation) {
        $this->messenger->addError($violation->getMessage());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function updateGroupsPermissions(string $group_permission_template_id) {
    $group_ids = \Drupal::entityQuery('group')->accessCheck(FALSE)
      ->condition('group_permission_template', $group_permission_template_id)
      ->execute();

    if (!empty($group_ids)) {
      /** @var \Drupal\group\Entity\Group[] $groups */
      $groups = $this->entityTypeManager->getStorage('group')->loadMultiple($group_ids);
      foreach($groups as $group) {
        $this->setTemplateAsGroupPermissions($group);
      }
    }
  }

}
