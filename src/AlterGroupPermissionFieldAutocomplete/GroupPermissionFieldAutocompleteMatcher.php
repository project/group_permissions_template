<?php

namespace Drupal\group_permissions_template\AlterGroupPermissionFieldAutocomplete;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Entity\EntityAutocompleteMatcher;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\group_permissions_template\Entity\GroupPermissionTemplate;

/**
 * Class GroupPermissionFieldAutocompleteMatcher.
 */
class GroupPermissionFieldAutocompleteMatcher extends EntityAutocompleteMatcher {

  /**
   * Drupal Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs an EntityAutocompleteMatcher object.
   *
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selection_manager
   *   The entity reference selection handler plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Drupal Entity type manager service.
   */
  public function __construct(
    SelectionPluginManagerInterface $selection_manager,
    EntityTypeManagerInterface $entity_type_manager) {

    parent::__construct($selection_manager);
    $this->entityTypeManager = $entity_type_manager;
  }
  /**
   * Gets matched labels based on a given search string.
   *
   * This class override the autocomplete search for grop_permission_template
   * field in order to provide only available templates for the current group
   * type.
   *
   */
  public function getMatches($target_type, $selection_handler, $selection_settings, $string = '') {
    if ($target_type === 'group_permission_template' && isset($selection_settings['group_bundle'])) {
      $matches = [];

      $options = $selection_settings + [
          'target_type' => $target_type,
          'handler' => $selection_handler,
        ];
      $handler = $this->selectionManager->getInstance($options);

      if (isset($string)) {
        // Get an array of matching entities.
        $match_operator = !empty($selection_settings['match_operator']) ? $selection_settings['match_operator'] : 'CONTAINS';
        $match_limit = isset($selection_settings['match_limit']) ? (int) $selection_settings['match_limit'] : 10;
        $entity_labels = $handler->getReferenceableEntities($string, $match_operator, $match_limit);

        if (!empty($entity_labels) && isset($entity_labels[$target_type])) {
          $template_ids = array_keys($entity_labels[$target_type]);
          $templates = $this->entityTypeManager->getStorage('group_permission_template')->loadMultiple($template_ids);
          // Loop through the entities and convert them into autocomplete output.
          foreach ($templates as $entity_id => $entity) {

            // Send only template from the good bundle.
            if ($entity->getType() === $selection_settings['group_bundle']) {
              $label = $entity->label();
              $key = "$label ($entity_id)";
              // Strip things like starting/trailing white spaces, line breaks and
              // tags.
              $key = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(Html::decodeEntities(strip_tags($key)))));
              // Names containing commas or quotes must be wrapped in quotes.
              $key = Tags::encode($key);
              $matches[] = ['value' => $key, 'label' => $label];
            }
          }
        }
      }
    }
    else {
      $matches = parent::getMatches($target_type, $selection_handler, $selection_settings, $string);
    }
    return $matches;
  }

}
