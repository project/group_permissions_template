<?php

namespace Drupal\group_permissions_template\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\group_permissions_template\Entity\GroupPermissionTemplateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class GroupPermissionsTemplateController extends ControllerBase {

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * GroupPermissionsTemplateController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *    Drupal Request stack service.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function redirectToEditForm(GroupPermissionTemplateInterface $group_permission_template) {
    $type = $group_permission_template->getType();
    $request = $this->requestStack->getCurrentRequest();
    $request->query->remove('destination');
    return $this->redirect('group_permissions_template.edit_form', [
      'group_type' => $type,
      'group_permission_template' => $group_permission_template->id(),
      ]);
  }

}
