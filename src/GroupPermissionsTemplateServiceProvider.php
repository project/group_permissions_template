<?php

namespace Drupal\group_permissions_template;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class GroupPermissionsTemplateServiceProvider.
 */
class GroupPermissionsTemplateServiceProvider extends ServiceProviderBase implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('entity.autocomplete_matcher');
    $definition->setClass('Drupal\group_permissions_template\AlterGroupPermissionFieldAutocomplete\GroupPermissionFieldAutocompleteMatcher');
    $definition->addArgument(new Reference('entity_type.manager'));
  }

}
